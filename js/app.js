(function($) {

    // Gallery carousel top wrapper
    var carouselTopWrapper = $('.gallery-thumbnails-wrapper .hidden'),
    // Gallery images container
        thumbnailsWrapper = $('#gallery-images'),
    // Gallery images list
        thumbnailsList = thumbnailsWrapper.find('.images-list'),
    // Thumbnail preview wrapper
        previewWrapper = $('.preview-image'),
    // Left thumbnails control
        controlPrevious = $('.pager .previous'),
    // Right thumbnails control
        controlNext = $('.pager .next'),
    // Overlay container
        overlay = $('.overlay'),
    // Overlay img wrapper
        overlayPreview = $('.overlay-preview'),
    // Overlay close button
        overlayClose = $('.overlay-close'),
    // Webpage top container
        container = $('.main-container');


    function resetGallery() {
        previewWrapper.empty();
        thumbnailsList.empty();
        carouselTopWrapper.empty();
    }

    // Set default search parameters or override them with those that user is looking for
    function getFormData() {
        var aifromVal = 'beatles', 
            ailimitVal = '10';
        
        if ($('#aifrom').val() != '') {
            aifromVal = $('#aifrom').val();
        }
        aifrom = '&aifrom=' + encodeURIComponent(aifromVal);

        if ($('#ailimit').val() != '') {
            ailimitVal = $('#ailimit').val();
        }
        ailimit = '&ailimit=' + encodeURIComponent(ailimitVal);

        return aifrom, ailimit;
    }

    function getWikiAPI(aifrom, ailimit) {
        var wikiAPI = 'https://en.wikipedia.org/w/api.php?action=query&list=allimages&aiprop=url&format=json' + aifrom + ailimit;
        return wikiAPI;
    }

    /* 
     * Generate requested number of images and replace src with loading gif
     * Wrap all images with span tag
     * Add 'selected' class to the first image thumbnail
    */
    function setInitialValues(i) {
        var selectedClass = (i == 0) ? ' selected' : '';
        $('<img/>')
            .attr('src', './images/spinner.gif')
            .appendTo(thumbnailsList)
            .wrap('<li class="loading' + selectedClass + '"></li>');
    }

    // Check returned file extension and replace it with blank image URL if it's incompatible with img tag
    function checkExtension(i, imgArr) {
        var imageExt = imgArr[i].url.toLowerCase().split('.').pop();
        if ((imageExt == 'ogg') || (imageExt == 'pdf') || (imageExt == 'xcf') || (imageExt == 'ogv')|| (imageExt == 'tif')) {
            imgArr[i].url = './images/blank.jpg';
        }
        return imgArr;
    }

    // This function performs controls behavior
    function thumbnailsNavigate(direction) {
        // Get current thumbnails list position
        var leftOffset = thumbnailsList.css('margin-left');
        var leftOffsetNumber = parseInt(leftOffset.substr(0, leftOffset.length - 2));
        // Get the width of visible thumbnails list
        var thumbnailsWrapperWidth = thumbnailsWrapper.outerWidth(true);
        // Get the full width of thumbnails list
        var thumbnailsListWidth = thumbnailsList.outerWidth(true);

        if ($(':animated').length) {
            return false;
        } else {
            if (Math.abs(leftOffsetNumber) >= 0) {
                if ((direction === 'right') && (thumbnailsList.outerWidth(true) > thumbnailsWrapper.outerWidth())) {
                    thumbnailsList.animate({
                        'margin-left' : leftOffsetNumber - thumbnailsWrapperWidth / 2 + 'px'
                    }, 300, function() {
                        var leftOffset = thumbnailsList.css('margin-left');
                        var leftOffsetNumber = parseInt(leftOffset.substr(0, leftOffset.length - 2));

                        if (leftOffsetNumber < 0) {
                            controlPrevious.removeClass('disabled');
                        }

                        if (thumbnailsList.outerWidth(true) < thumbnailsWrapper.outerWidth()) {
                            controlNext.addClass('disabled');
                        }
                    });
                } else if ((direction === 'left') && (leftOffsetNumber < 0)) {
                    thumbnailsList.animate({
                        'margin-left' : leftOffsetNumber + thumbnailsWrapperWidth / 2 + 'px'
                    }, 300, function() {
                        var leftOffset = thumbnailsList.css('margin-left');
                        var leftOffsetNumber = parseInt(leftOffset.substr(0, leftOffset.length - 2));

                        if (Math.abs(leftOffsetNumber) <= 0) {
                            controlPrevious.addClass('disabled');
                        } else {
                            controlNext.removeClass('disabled');
                        }

                    });
                } else {
                    return false;
                }
            }
        }
    }

    // Function is used to toggle overlay mode
    function toggleOverlay() {
        if (overlay.hasClass('open')) {
            overlay.removeClass('open');
            container.removeClass('overlay-open');
        } else if (!overlay.hasClass('open')) {
            overlay.addClass('open');
            container.addClass('overlay-open');
        }
    }

    // This function is called when all images are loaded
    function imageLoadedCallback() {
        // Count thumbnails list outerWidth
        var arr = thumbnailsList.find('li');
        var ulWidth = 0;
        arr.each(function() {
            ulWidth += Math.ceil($(this).outerWidth(true));
        });
        ulWidth += 1; // By some reason sometimes the whole width is 1px narrower than it's needed
        thumbnailsList.css('width', ulWidth);

        // Activate/disable right control depends on ratio between list's total width and it's visible area
        if (thumbnailsList.outerWidth(true) > thumbnailsWrapper.outerWidth()) {
            controlNext.removeClass('disabled');
        } else {
            if (!(controlNext.hasClass('disabled'))) {
                controlNext.addClass('disabled');
            }
        }

        // Overlay open/close events
        previewWrapper.on('click', function(e) {
            toggleOverlay();
            overlayPreview.empty();
            $('<img/>')
                .attr({
                    'src' : $(this).find('img')[0].src
                })
                .appendTo('.overlay-preview')
                .on('load', function() {
                    overlayPreview.find('img')
                        .css('left', function() {
                            return ((window.innerWidth - $(this).outerWidth(true)) / 2);
                        })
                        .css('top', function() {
                            return ((window.innerHeight - $(this).outerHeight(true)) / 2);
                        });
                });
        });
        overlayClose.on('click', toggleOverlay);
    }


    /*
     * fn extension which is applied on a list of image objects and calls
     * the specified callback function, only when all of them are loaded
    */
    $.fn.imagesLoaded = function(options) {
        var images = $(this);
        var originalTotalImagesCount = images.size();
        var totalImagesCount = originalTotalImagesCount;
        var elementsLoaded = 0;

        images.each(function() {
            // The image has already been loaded
            if ($(this)[0].complete) {
                totalImagesCount--;
            } else {
                $(this).load(function() {
                    elementsLoaded++;

                    // An image has been loaded
                    if (elementsLoaded >= totalImagesCount) {
                        if (options.loadingCompleteCallback) {
                            options.loadingCompleteCallback();
                        }
                    }
                });
                $(this).error(function() {
                    elementsLoaded++;

                    // An image has been loaded
                    if (elementsLoaded >= totalImagesCount) {
                        if (options.loadingCompleteCallback) {
                            options.loadingCompleteCallback();
                        }
                    }
                });
            }
        });

        // There are no unloaded images
        if (totalImagesCount <= 0) {
            if (options.loadingCompleteCallback) options.loadingCompleteCallback();
        }
    };

    // Form submit
    $('#target').on('submit', function(event) {
        // Reset left margin of thumbnails list
        thumbnailsList.css('margin-left', '0px');

        // Show the wrapper of gallery thumbnails list
        if ($('.gallery-thumbnails-wrapper:hidden')) {
            $('.gallery-thumbnails-wrapper').fadeIn('fast');
        }

        getFormData();

        $.ajax({
            type: 'GET',
            url: getWikiAPI(aifrom, ailimit),
            async: false,
            jsonpCallback: 'jsonCallback',
            contentType: 'application/json',
            dataType: 'jsonp',
            success: function(result) {
                resetGallery();

                // Add all returned image objects to an array
                var imgArr = result.query.allimages;
                var imgCount = imgArr.length;

                for (var i = 0; i < imgCount; i++) {
                    setInitialValues(i);
                    checkExtension(i, imgArr);
                }

                $.each(imgArr, function(i, image) {
                    // Images preloading. Add all returned by API call images to the hidden div
                    $('<img/>')
                        .attr('src', image.url)
                        .appendTo(carouselTopWrapper)
                        .on('load', function() {
                            // Update appropriate thumbnail src with necessary one on every image finished loading
                            thumbnailsList.find('li')[i].setAttribute('class', '');
                            thumbnailsList.find('img')[i].setAttribute('src', this.src);

                            // Set 'selected' class for the first image and load it with preview wrapper
                            if (i == 0) {
                                thumbnailsList.find('li')[i].setAttribute('class', 'selected');
                                $('<img/>')
                                    .attr({
                                        'src' : image.url,
                                        'class' : 'img-responsive'
                                    })
                                    .appendTo(previewWrapper);

                                previewWrapper.fadeIn('slow');
                            }
                        });
                });
                
                // Called imagesLoaded function that checks images loading progress
                thumbnailsList.find('img').imagesLoaded({
                    loadingCompleteCallback: imageLoadedCallback
                });
            },
            error: function(e) {
                alert('Something went wrong!');
            }
        });
        event.preventDefault();
    });

    // Show image preview on thumbnail click
    thumbnailsWrapper.on('click', 'img', function() {
        if (!($(this).parent().hasClass('loading'))) {
            $(this).parent()
                .addClass('selected')
                .siblings()
                    .removeClass('selected');
            var thumbnailSource = this.src;
            var imgPreview = previewWrapper.find('img');
            var imgPreviewSrc = imgPreview.attr('src');

            if (thumbnailSource != imgPreviewSrc) {
                imgPreview
                    .addClass('preview-animation')
                    .fadeOut('slow', function() {
                        imgPreview.attr('src', thumbnailSource);
                        imgPreview.fadeIn('slow');
                        imgPreview.removeClass('preview-animation');
                    });
            }
        }
    });

    // Initialize navigation events
    controlNext.on('click', function() {
        thumbnailsNavigate('right');
    });
    controlPrevious.on('click', function() {
        thumbnailsNavigate('left');
    });

    // Navigate to the previous image under the overlay
    function goPrevious(selectedIndex, imagesCount) {
        // Update thumbnails active element
        thumbnailsList.find('li')[selectedIndex].className = '';
        thumbnailsList.find('li')[imagesCount - 1].className = 'selected';
        // Update preview image
        previewWrapper.find('img').attr('src', thumbnailsList.find('img')[imagesCount - 1].src);
        // Update overlay image
        overlayPreview.find('img')
            .addClass('overlay-transition-left')
            .fadeOut('slow', function() {
                overlayPreview.find('img').attr('src', thumbnailsList.find('img')[imagesCount - 1].src);
                overlayPreview.find('img').fadeIn('fast');
                overlayPreview.find('img').removeClass('overlay-transition-left');
            });
    }

    // Navigate to the next image under the overlay
    function goNext(selectedIndex) {
        thumbnailsList.find('li')[selectedIndex].className = '';
        thumbnailsList.find('li')[selectedIndex + 1].className = 'selected';

        previewWrapper.find('img').attr('src', thumbnailsList.find('img')[selectedIndex + 1].src);

        overlayPreview.find('img')
            .addClass('overlay-transition-right')
            .fadeOut('slow', function() {
                overlayPreview.find('img').attr('src', thumbnailsList.find('img')[selectedIndex + 1].src);
                overlayPreview.find('img').fadeIn('fast');
                overlayPreview.find('img').removeClass('overlay-transition-right');
            });
    }

    // Trigger key press with overlay images
    $("body").keydown(function(e) {
        if (overlay.hasClass('open')) {
            var selectedIndex = thumbnailsList.find('li.selected').index();
            var imagesCount = thumbnailsList.find('li').length;

            if (e.keyCode == 37) { // left arrow pressed
                if (selectedIndex === 0) {
                    goPrevious(selectedIndex, imagesCount);
                } else {
                    goPrevious(selectedIndex, selectedIndex);
                }

            } else if (e.keyCode == 39) { // right arrow pressed
                if (selectedIndex === thumbnailsList.find('li').length - 1) {
                    thumbnailsList.find('li')[selectedIndex].className = '';
                    thumbnailsList.find('li')[0].className = 'selected';

                    previewWrapper.find('img').attr('src', thumbnailsList.find('img')[0].src);

                    overlayPreview.find('img')
                        .addClass('overlay-transition-right')
                        .fadeOut('slow', function() {
                            overlayPreview.find('img').attr('src', thumbnailsList.find('img')[0].src);
                            overlayPreview.find('img').fadeIn('fast');
                            overlayPreview.find('img').removeClass('overlay-transition-right');
                        });
                } else {
                    goNext(selectedIndex);
                }

            } else if (e.keyCode == 27) { // Esc pressed
                toggleOverlay();
            }
        }
    });

})(jQuery);

