# Wiki Image Gallery

> Author : Taras Fedkiv

> Date   : September 2014

#### Initial requirements

1. retrieve images using MediaWiki API
1. present them in a way that proves my UX and Frontend skills
1. have two presentation modes. One picture view and an multiple pictures view.
1. work fluently independent on the images sizes and amounts
1. responsive
1. visually attractive
1. work on popular browsers